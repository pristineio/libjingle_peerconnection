#
# Be sure to run `pod lib lint libjingle_peerconnection.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "libjingle_peerconnection"
  s.version          = "6815.0"
  s.summary          = "An iOS WebRTC demo application hosted on App Engine. Builds by Pristine.io"
  s.description      = <<-DESC
                       The WebRTC native APIs are implemented based on the following [WebRTC spec.](http://dev.w3.org/2011/webrtc/editor/webrtc.html) 

                       The code that implements WebRTC native APIs (including the Stream and the PeerConnection APIs) are available in [libjingle](https://code.google.com/p/libjingle/source/browse/#svn%2Ftrunk%2Ftalk%2Fapp%2Fwebrtc). A [sample client application](https://code.google.com/p/libjingle/source/browse/#svn%2Ftrunk%2Ftalk%2Fexamples%2Fpeerconnection%2Fclient) is also provided there. 

                       The target audience of this document are those who want to use WebRTC Native APIs to develop native RTC applications.
                       DESC
  s.homepage         = "https://bitbucket.org/pristineio/libjingle_peerconnection.git"
  s.screenshots     = "https://bytebucket.org/pristineio/libjingle_peerconnection/raw/7c9cd5c64496148b7b268445ad9722aca376b7c9/iphone-dev.png", "https://bytebucket.org/pristineio/libjingle_peerconnection/raw/7c9cd5c64496148b7b268445ad9722aca376b7c9/ipad-sim.png"
  s.license          = 'http://www.webrtc.org/license-rights/license'
  s.author           = { "Rahul Behera" => "rahul@pristine.io" }
  s.source           = { :git => "https://bitbucket.org/pristineio/libjingle_peerconnection.git", :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/bot_the_builder'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes'
  # s.resources = 'Pod/Assets/*.png'

  s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'AVFoundation', 'CoreGraphics', 'CoreMedia', 'GLKit', 'UIKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  s.libraries = 'c', 'sqlite3', 'stdc++.6.0.9'
  s.vendored_libraries = 'Libraries/libWebRTC-6815-armv7-ia32-Debug.a'
end
